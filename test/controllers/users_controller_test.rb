require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  
  def setup
    @user = users(:michael)
    @user1 = users(:archer)
  end
  
  test "should get new" do
    get :new
    assert_response :success
  end
  
  test "should check edit action without before_action" do
    get :edit, id: @user
    assert_not flash.empty?
    assert_redirected_to login_url
    
  end
  
  test "should check update action without before_action" do
    patch :update, id: @user, user: {name:@user.name,email:@user.email}
    assert_not flash.empty?
    assert_redirected_to login_url
  end
  
  test "should check user can only edit his own profile" do
    log_in_as(@user)
    get :edit, id: @user1
    assert flash.empty?
    assert_redirected_to root_url
  end
  
  test "should check user can only update his own profile" do
    log_in_as(@user)
    patch :update,id: @user1, user: {name:'mich',email:'mich@gmail.com'}
    assert flash.empty?
    assert_redirected_to root_url
  end
  
  test "redirected to index page" do
    get :index
    assert_redirected_to login_url
  end
  
  test "should not delete user without login" do
    assert_no_difference 'User.count' do
      delete :destroy,id:@user
    end
    assert_redirected_to login_url
  end
  
  test "should redirect to home page if user is not admin" do
    log_in_as(@user1)
    assert_no_difference 'User.count' do
      delete :destroy, id:@user
    end
    assert_redirected_to root_url
  end
  
  test "should not allow to update user as admin through web" do
    log_in_as(@user1)
    patch :update, id: @user1, user: {password: @user1.password,password_confirmation: @user1.password,admin: true}
    assert_not @user1.admin?
  end
  
  test "should not allow following page without login" do
    get :following, id:@user
    assert_redirected_to login_url
  end
  
  test "should not allow followers page without login" do
    get :followers, id:@user
    assert_redirected_to login_url
  end
  
  

end
