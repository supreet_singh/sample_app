require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(name:"Lewis", email:"lewis.rogers@gmail.com", password:"foobar",password_confirmation:"foobar")
  end
  
  test "should validate User" do
    assert @user.valid?
  end
  
  test "should validate name presence" do 
    @user.name ="    "
    assert_not @user.valid?
  end
  
  test "should validate email presence" do
    @user.email = "   "
    assert_not @user.valid?
  end
  
  test "name should not exceed length 50" do
    @user.name = "a" * 51
    assert_not @user.valid?
  end
  
  test "should accept right format email" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"
    end
  end
  
  test "should reject wrong format email" do
    invalid_addresses = %w[user@example foo@bar..com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
    end
    
  end
  
  test "should check email is unique" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end
  
  test "password should have minimum length 6" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end
  
  test "email should be downcase before inserting to db" do
    mix_email = "FooBar@example.com"
    @user.email = mix_email
    @user.save
    assert_equal mix_email.downcase, @user.reload.email
  end
  
  test "authenticated? should return false if remember_digest is nil" do
    assert_not @user.authenticated?(:remember,'')
  end
  
  test "micropost corresponding to user should be destroyed" do
    @user.save
    @user.microposts.create!(content: "first post")
    assert_difference 'Micropost.count', -1 do
      @user.destroy
    end
  end
  
  test "user should follow and unfollow users" do
    michael = users(:michael)
    archer = users(:archer)
    assert_not michael.following?(archer)
    michael.follow(archer)
    assert michael.following?(archer)
    michael.unfollow(archer)
    assert_not michael.following?(archer)
    assert_not archer.followers.include?(michael)
    michael.follow(archer)
    assert archer.followers.include?(michael)
  end
  
  test "should have the right posts" do
    michael = users(:michael)
    archer = users(:archer)
    lana = users(:lana)
    
    lana.microposts.each do |lana_microposts|
      assert michael.feed.include?(lana_microposts)
    end
    
    michael.microposts.each do |self_posts|
      assert michael.feed.include?(self_posts)
    end
    
    archer.microposts.each do |other_posts|
      assert_not michael.feed.include?(other_posts)
    end
  end
end
