require 'test_helper'

class MicropostTest < ActiveSupport::TestCase
  
  def setup
    @user = users(:michael)
    @micropost = @user.microposts.build(content: "helloworld")
  end
  
  test "user should be valid" do
    assert @micropost.valid?
  end
  
  test "user id should be present" do
    @micropost.user_id = nil
    assert_not @micropost.valid?
  end
  
  test "micropost content should be present" do
    @micropost.content = " "
    assert_not @micropost.valid?
  end
  
  test "micropost should be only upto 140 chars" do
    @micropost.content = "a" * 141
    assert_not @micropost.valid?
  end
  
  test "first micropost is most recent one" do
    micropost = microposts(:most_recent)
    assert_equal Micropost.first, micropost
  end
end
