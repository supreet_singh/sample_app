require 'test_helper'

class PasswordResetsTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users("michael")
    ActionMailer::Base.deliveries.clear
  end
  
  test "should verify password reset working" do
    get new_password_reset_path
    assert_template 'password_resets/new'
    post password_resets_path, password_reset: { email: " " }
    assert_not flash.empty?
    assert_template 'password_resets/new'
    post password_resets_path, password_reset: {email: @user.email}
    assert_not flash.empty?
    assert_not_equal @user.reset_digest, @user.reload.reset_digest
    assert_equal 1, ActionMailer::Base.deliveries.size
    assert_redirected_to root_url
    user = assigns(:user)
    get edit_password_reset_path(user.reset_token,email:"")
    assert_redirected_to root_url
    get edit_password_reset_path(user.reset_token,email:"wrong")
    assert_redirected_to root_url
    get edit_password_reset_path("wrong",email: user.email)
    assert_redirected_to root_url
    get edit_password_reset_path(user.reset_token, email: user.email)
    assert_template 'password_resets/edit'
    assert_select 'input[name=email][type=hidden][value=?]', user.email
    patch password_reset_path(user.reset_token), email: user.email, user: { password:"foobar", password_confirmation:"abcdef" }
    assert_select 'div#error_explanation'
   patch password_reset_path(user.reset_token),
          email: user.email,
          user: { password:              "  ",
                  password_confirmation: "foobar" }
    assert_not flash.empty?
    assert_template 'password_resets/edit'
    patch password_reset_path(user.reset_token),
          email: user.email,
          user: { password:              "foobaz",
                  password_confirmation: "foobaz" }
    assert_not flash.empty?
    assert is_logged_in?
    assert_redirected_to user
    
  end
end
