require 'test_helper'


class UsersIndexTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:michael)
    @user1 = users(:archer)
  end

  test "user index test" do
    log_in_as(@user)
    get users_path
    assert_template 'users/index'
    assert_select 'div.pagination'
    User.paginate(page: 1).each do |user|
      assert_select 'a[href=?]', user_path(user), text: user.name
    end
  end
  
  test "admin should be able to delete non-admin" do
    log_in_as(@user)
    get users_path
    assert_template 'users/index'
    assert_select 'div.pagination'
    firstpage = User.paginate(page:1)
    firstpage.each do |user|
      assert_select 'a[href=?]', user_path(user), text: user.name
      unless user == @user
        assert_select 'a[href=?]', user_path(user),text: 'delete',method: :delete
      end
    end
    
    assert_difference 'User.count', -1 do
      delete user_path(@user1)
    end
    
  end
  
  test "should check no delete links on non-admin" do
    log_in_as(@user1)
    get users_path
    assert_select 'a',text: 'delete',count: 0
  end
end


 