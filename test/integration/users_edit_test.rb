require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  def setup
    #take sample user data from yml file
    @user = users(:michael)
  end
  
  test "should validate invalid updates" do
    log_in_as(@user)
    get edit_user_path(@user) 
    assert_template 'users/edit'
    patch user_path(@user) , user: {name: "", email:"invalid@gmail.com", password: 'foo', password_confirmation: 'bar'}
    assert_template 'users/edit'
  end
  
  test "should edit valid user" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    name = "Supreet"
    email = "ssupreet.14@gmail.com"
    patch user_path(@user), user: {name:name, email:email, password: '',password_confirmation: ''}
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    assert_equal @user.name, name
    assert_equal @user.email, email
    
  end
  
  test "successful edit with friendly forwarding" do
    get edit_user_path(@user)
    log_in_as(@user)
    assert_redirected_to edit_user_path(@user)
    
  end
  
  test "forwarding-url stays only for one login" do
    get edit_user_path(@user)
    log_in_as(@user)
    assert_redirected_to edit_user_path(@user)
    assert session[:forwarding_url] == nil
    logout_test(@user)
    delete logout_path
    log_in_as(@user)
    assert_redirected_to @user
  end
end
