require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:michael)
  end
  
  test "layout_links" do
    get root_path
    assert_template 'static_pages/home'
    assert_select "a[href=?]",root_path, count:2
    assert_select "a[href=?]",about_path
    assert_select "a[href=?]",contact_path
    assert_select "a[href=?]",help_path
    end
  
  test "signup layout title" do
    get signup_path
    assert_response :success
    assert_select 'title',full_title("Sign Up")
  end
  
  test "layout after login" do
    get login_path
    log_in_as(@user)
    assert_redirected_to @user
    follow_redirect!
    assert_select 'a[href=?]', users_path
    assert_select 'a[href=?]', logout_path
    assert_select 'a[href=?]', user_path(@user)
    assert_select 'a[href=?]', edit_user_path(@user)
  end
  
  test "layout for non-logged in users" do
    get edit_user_path(@user)
    assert_redirected_to login_path
    log_in_as(@user)
    assert_redirected_to edit_user_path(@user)
    follow_redirect!
  end
end

