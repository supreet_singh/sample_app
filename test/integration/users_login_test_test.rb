require 'test_helper'

class UsersLoginTestTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:michael)
  end
  
  test "should test user with valid login" do
    get login_path
    post login_path, session: {email:@user.email,password:'password'}
    assert_redirected_to @user
    follow_redirect!
    assert_template 'users/show'
    assert_select 'a[href = ?]',login_path, count:0
    assert_select 'a[href = ?]', logout_path
    assert_select 'a[href= ?]',user_path(@user)
    delete logout_path
    assert_not is_logged_in?
    assert_redirected_to root_url
    follow_redirect!
    assert_select 'a[href = ?]',login_path
    assert_select 'a[href= ?]',logout_path,count:0
    assert_select 'a[href = ?]',user_path(@user),count:0
    
    #user logged in multiple windows
    delete logout_path
    assert_not is_logged_in?
    assert_redirected_to root_url
    follow_redirect!
    assert_select 'a[href =?]', login_path
    assert_select 'a[href =?]',logout_path, count:0
    assert_select 'a[href= ?]',user_path(@user),count:0
    
    end
    
    test "should store cookies" do
      log_in_as(@user,remember_me: '1')
      assert_equal assigns(:user).remember_token, cookies['remember_token']
    end
    
    test "should not store cookies" do
      log_in_as(@user,remember_me: '0')
      assert_nil cookies['remember_token']
    end
end
