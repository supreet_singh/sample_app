# Ruby on Rails Tutorial: sample application

This is a side project i made using ruby on rails. I have build a user login system, added functionality for sessions, cookies and user authentication from scratch without using
any gems like 'devise'. The project also uses complex relationships and associations between various models such as Users, Followers, Followings. For front end i have mostly 
used the twitter bootstrap gem. The project also uses some gems like carrierwave(for picture uploading), fog(for using aws to host images), faker(for generating fake data for unit 
and integration testing). The App is completely test driven with test cases for layouts, controllers as well as database models. In its current functionality, the app allows a user 
to sign up, receive activation email, view account, create microposts, upload images, view and follow other users, edit account settings,reset password, admin access to delete users.
The App is hosted to heroku at https://rocky-plateau-4273.herokuapp.com/